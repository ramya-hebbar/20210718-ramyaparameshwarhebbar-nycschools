//
//  DatabaseHandler.m
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/18/21.
//

#import "DatabaseHandler.h"
#import "School.h"
#import "AppConstants.h"

static DatabaseHandler *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DatabaseHandler {
    NSString * dbPath;
}

+ (instancetype) sharedHandler {
    static DatabaseHandler * sharedDatabaseHandler = nil;

        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedDatabaseHandler = [[self alloc] init];
        });
    
        return sharedDatabaseHandler;
}

- (instancetype) init {
    if (self = [super init]) {
        
    }
    NSLog(@"DatabaseHandler initialized");
    return self;
}

-(void) initializeDB {
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    dbPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"schools.db"]];
    //       BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: dbPath] == NO) {
        const char *dbpath = [dbPath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
            char *errMsg;
            
            //creating the tables
            
            const char *school_sql_stmt = "CREATE TABLE IF NOT EXISTS SCHOOL (dbn TEXT PRIMARY KEY NOT NULL, school_name TEXT, overview_paragraph TEXT, city TEXT, neighborhood TEXT, phone_number TEXT, fax_number TEXT, school_email TEXT, website TEXT, primary_address_line_1 TEXT, zip TEXT, state_code TEXT)";
            
            if (sqlite3_exec(database, school_sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                NSLog(@"Failed to create School table");
            }
                        
            sqlite3_close(database);
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
}

-(BOOL) insertSchoolData: (NSDictionary *) dictionary {
    const char *dbpath = [dbPath UTF8String];
       
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")",
                               tableSchool,
                               fieldDBN,
                               fieldSchoolname,
                               fieldOverview,
                               fieldCity,
                               fieldNeighborhood,
                               fieldPhone,
                               fieldFax,
                               fieldEmail,
                               fieldWebsite,
                               fieldAddress,
                               fieldZip,
                               fieldState,
                               [dictionary valueForKey:fieldDBN],
                               [dictionary valueForKey:fieldSchoolname],
                               [dictionary valueForKey:fieldOverview],
                               [dictionary valueForKey:fieldCity],
                               [dictionary valueForKey:fieldNeighborhood],
                               [dictionary valueForKey:fieldPhone],
                               [dictionary valueForKey:fieldFax],
                               [dictionary valueForKey:fieldEmail],
                               [dictionary valueForKey:fieldWebsite],
                               [dictionary valueForKey:fieldAddress],
                               [dictionary valueForKey:fieldZip],
                               [dictionary valueForKey:fieldState]];
           
          const char *insert_stmt = [insertSQL UTF8String];
          
           sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
          
          if (sqlite3_step(statement) == SQLITE_DONE) {
              NSLog(@"New record inserted");
             return YES;
          } else {
              NSLog(@"Issue with record insert");
             return NO;
          }
       }
    
    NSLog(@"Issue opening Database");
    return NO;
}

-(NSArray *) querySchoolData {
   const char *dbpath = [dbPath UTF8String];
   
   if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
      NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM %@", tableSchool];
       
      const char *query_stmt = [querySQL UTF8String];
      
       NSMutableArray *resultArray = [[NSMutableArray alloc]init];
      
      if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
         while (sqlite3_step(statement) == SQLITE_ROW) {
             
             School * school = [[School alloc] init];
             
             school.schoolID = [[NSString alloc] initWithUTF8String:
                                (const char *) sqlite3_column_text(statement, 0)];
             
             school.name = [[NSString alloc] initWithUTF8String:
                            (const char *) sqlite3_column_text(statement, 1)];
             
             school.overview = [[NSString alloc] initWithUTF8String:
                                (const char *) sqlite3_column_text(statement, 2)];
             
             school.city = [[NSString alloc] initWithUTF8String:
                            (const char *) sqlite3_column_text(statement, 3)];
             
             school.neighborhood = [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 4)];
             
             school.phoneNumber = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 5)];
             
             school.faxNumber = [[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 6)];
             
             school.email = [[NSString alloc] initWithUTF8String:
                             (const char *) sqlite3_column_text(statement, 7)];
             
             school.website = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 8)];
             
             school.address = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 9)];
             
             school.zipcode = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 10)];
             
             school.state = [[NSString alloc] initWithUTF8String:
                             (const char *) sqlite3_column_text(statement, 11)];

             [resultArray addObject:school];
         }
          
         sqlite3_reset(statement);
          
          return resultArray;
      }
   }
   return nil;
}

- (void) clearSchoolData {
    
    const char *dbpath = [dbPath UTF8String];
       
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString * deleteSQL = [NSString stringWithFormat:@"DELETE FROM %@", tableSchool];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
         sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"Record deleted");
        } else {
            NSLog(@"Issue with record delete");
        }
    }
    else {
        NSLog(@"Issue opening Database");
    }
}

@end
