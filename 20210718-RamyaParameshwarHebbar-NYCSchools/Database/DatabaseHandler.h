//
//  DatabaseHandler.h
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/18/21.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

/*
 This class is responsible for performing CRUD operations on the local SQLite database.
 */

NS_ASSUME_NONNULL_BEGIN

@interface DatabaseHandler : NSObject 

@property (class, readonly, strong) DatabaseHandler *sharedHandler NS_SWIFT_NAME(shared);

/*
 This method checks and initializes database and creates all tables required if they don't exist.
 */
-(void) initializeDB;

/*
 This method inserts records for the school list availed.
 */
-(BOOL) insertSchoolData: (NSDictionary *) dictionary;

/*
 This method fetches list of all schools.
 */
-(NSArray *) querySchoolData;

/*
 This method deletes all the existing records from School table.
 */
-(void) clearSchoolData;

@end

NS_ASSUME_NONNULL_END


