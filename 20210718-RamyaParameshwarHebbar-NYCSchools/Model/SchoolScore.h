//
//  SchoolScore.h
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/19/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SchoolScore : NSObject

@property(nonatomic, strong) NSString * schoolID;
@property(nonatomic, strong) NSString * readingScore;
@property(nonatomic, strong) NSString * mathScore;
@property(nonatomic, strong) NSString * writingScore;

@end

NS_ASSUME_NONNULL_END
