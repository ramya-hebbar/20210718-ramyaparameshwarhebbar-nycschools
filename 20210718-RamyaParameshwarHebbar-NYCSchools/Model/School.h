//
//  School.h
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/19/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface School : NSObject

@property(nonatomic, strong) NSString * schoolID;
@property(nonatomic, strong) NSString * name;
@property(nonatomic, strong) NSString * overview;
@property(nonatomic, strong) NSString * neighborhood;
@property(nonatomic, strong) NSString * phoneNumber;
@property(nonatomic, strong) NSString * faxNumber;
@property(nonatomic, strong) NSString * email;
@property(nonatomic, strong) NSString * website;
@property(nonatomic, strong) NSString * address;
@property(nonatomic, strong) NSString * city;
@property(nonatomic, strong) NSString * state;
@property(nonatomic, strong) NSString * zipcode;




@end

NS_ASSUME_NONNULL_END
