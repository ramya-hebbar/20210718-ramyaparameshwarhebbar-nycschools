//
//  AppConstants.h
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/19/21.
//

#ifndef AppConstants_h
#define AppConstants_h

static NSString * const baseURL = @"https://data.cityofnewyork.us/resource/";
static NSString * const schoolsOffset = @"s3k6-pzi2.json";
static NSString * const scoresOffset = @"f9bf-2cp4.json";

static NSString * const fieldDBN = @"dbn";
static NSString * const fieldSchoolname = @"school_name";
static NSString * const fieldOverview = @"overview_paragraph";
static NSString * const fieldCity = @"city";
static NSString * const fieldNeighborhood = @"neighborhood";
static NSString * const fieldPhone = @"phone_number";
static NSString * const fieldFax = @"fax_number";
static NSString * const fieldEmail = @"school_email";
static NSString * const fieldWebsite = @"website";
static NSString * const fieldAddress = @"primary_address_line_1";
static NSString * const fieldZip = @"zip";
static NSString * const fieldState = @"state_code";
static NSString * const fieldScoreMath = @"sat_math_avg_score";
static NSString * const fieldScoreRead = @"sat_critical_reading_avg_score";
static NSString * const fieldScoreWrite = @"sat_writing_avg_score";

static NSString * const querySelect = @"$select=";
static NSString * const queryOrderBy = @"$order=";

static NSString * const tableSchool = @"SCHOOL";

#endif /* AppConstants_h */
