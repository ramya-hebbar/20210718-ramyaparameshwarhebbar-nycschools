//
//  DetailViewController.swift
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/19/21.
//

import UIKit

/*
 This class displays details for a selected school.
 
 Some improvizations that could have been done with extra time are:
 1. Refresh logic in case of network failure (similar to list screen)
 2. Linkifying the options under Other Information -
 Address -> Maps, Phone -> Dialer, Email -> Email client, Website -> Browser
 */

private let dispatchQueue = DispatchQueue(label: "My Detail Dispatch Queue")

class DetailViewController: UIViewController, UITableViewDataSource {

    @IBOutlet var tableView : UITableView!
    
    var schoolInfo : School!
    
    var scoreInfo : SchoolScore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //Add wait indicator
        let busyIndicator : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 100 ,y: 200, width: 100, height: 100)) as UIActivityIndicatorView
        busyIndicator.center = self.view.center
        busyIndicator.hidesWhenStopped = true
        busyIndicator.style = UIActivityIndicatorView.Style.large
        busyIndicator.startAnimating()
        self.view.addSubview(busyIndicator)
        
        //Making the API call to fetch scores data for selected school
        dispatchQueue.async { [weak self] in
            let handler = JsonDataHandler.shared
            let schoolScore : SchoolScore? =  handler.fetchSchoolDetails(for: (self?.schoolInfo.schoolID)!)

            guard let info = schoolScore else {
                print("Error fetching score data")
                
                DispatchQueue.main.async {[weak self] in
                    //Update UI on main thread
                    busyIndicator.stopAnimating()
                    
                    let alertView : UIAlertController = UIAlertController.init(title: "Error",
                                                                               message: "Issue fetching data, please try later.", preferredStyle: UIAlertController.Style.alert)
                    let action : UIAlertAction = UIAlertAction.init(title: "OK",
                                                                    style: UIAlertAction.Style.default) { (UIAlertAction) in
                        self?.navigationController?.popViewController(animated: true)
                    }
                    
                    alertView.addAction(action)
                    
                    self?.present(alertView, animated: true, completion: nil)
                }
                return
            }

            self?.scoreInfo = info

            DispatchQueue.main.async {[weak self] in
                //Update UI on main thread
                busyIndicator.stopAnimating()
                self?.tableView.reloadData()
            }
        }
    }

    // MARK: Tableview methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard scoreInfo != nil else {
            return 0
        }

        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        case 1:
            return 3
            
        case 2:
            return 5
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCellID")!
        
        cell.textLabel?.numberOfLines = 0
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = ""
        
        switch indexPath.section {
        case 0: // School intro
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = schoolInfo.name
                cell.detailTextLabel?.text = schoolInfo.overview
                
            default:
                break
            }
            
        case 1: // SAT scores
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Math Score: \(scoreInfo.mathScore)"
                
            case 1:
                cell.textLabel?.text = "Reading Score: \(scoreInfo.readingScore)"
                
            case 2:
                cell.textLabel?.text = "Writing Score: \(scoreInfo.writingScore)"
                
            default:
                break
            }
            
        case 2: //Other details
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Address: \(schoolInfo.address) \(schoolInfo.city) \(schoolInfo.state) \(schoolInfo.zipcode)"
                
            case 1:
                cell.textLabel?.text = "Phone: \(schoolInfo.phoneNumber)"
                
            case 2:
                cell.textLabel?.text = "Website: \(schoolInfo.website)"
                
            case 3:
                cell.textLabel?.text = "Email: \(schoolInfo.email)"
                
            case 4:
                cell.textLabel?.text = "Fax: \(schoolInfo.faxNumber)"
                
            default:
                break
            }
            
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "General Information"
            
        case 1:
            return "Scores"
            
        case 2:
            return "Other Information"
            
        default:
            return ""
        }
    }
}
