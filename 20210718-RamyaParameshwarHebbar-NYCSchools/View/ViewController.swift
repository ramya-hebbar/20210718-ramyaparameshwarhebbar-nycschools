//
//  ViewController.swift
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/18/21.
//

import UIKit

private let dispatchQueue = DispatchQueue(label: "My List Dispatch Queue")

/*
 This class displays the initial list of NYC schools.
 
 Some explanation on my thoughts regarding for/against pagination.

 Option One - Use Database: We can take advantage of the local SQLite database to host the list of all schools.
 With more time, I could have added a logic to fetch school list once in 24 hours or so.
 With more time, I would paginate query from local db. (Example, lazy load 20 records at a time from local db)

 Option Two - Use Pagination: I can alternatively use pagination to fetch the list of schools, like fetch 20 items at a time.
 Logic would be to check if the list item index is mod of 20 and fetch next set of schools achieving lazy loading.
 https://soda.demo.socrata.com/resource/4tka-6guv.json?$limit=20&$offset=index

 I have decided to proceed using database assuming that number of schools will not change frequently. It also makes the app responsive by harnessing the local SQLite database.
 
 Note: Option Three - Hybrid of Database and Pagination
 */

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView : UITableView!
    
    var listOfSchools = [School]()
    
    var selectedRow = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "NYC Schools"
                
        //Make the API call to fetch NYC schools data
        requestDataFromApi()
    }
    
    // MARK: Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID")!
        
        let school = listOfSchools[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = school.name
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = school.neighborhood
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "detailScreenSegue", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let dvc = segue.destination as? DetailViewController {
            dvc.schoolInfo = listOfSchools[selectedRow]
            dvc.modalPresentationStyle = .fullScreen
        }
    }
    
    // MARK: Data fetch method
    
    func requestDataFromApi() {
        //Add wait indicator
        let busyIndicator : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 100 ,y: 200, width: 100, height: 100)) as UIActivityIndicatorView
        busyIndicator.center = self.view.center
        busyIndicator.hidesWhenStopped = true
        busyIndicator.style = UIActivityIndicatorView.Style.large
        busyIndicator.startAnimating()
        self.view.addSubview(busyIndicator)
        
        dispatchQueue.async {[weak self] in
            let dataHandler = JsonDataHandler.shared
            let flag = dataHandler.fetchSchoolsData()
            
            if !flag {
                //error
                DispatchQueue.main.async {[weak self] in
                    //Update UI on main thread
                    busyIndicator.stopAnimating()
                    
                    let alertView : UIAlertController = UIAlertController.init(title: "Error",
                                                                               message: "Issue fetching data, would you like to retry?", preferredStyle: UIAlertController.Style.alert)
                    
                    let actionYes : UIAlertAction = UIAlertAction.init(title: "Yes",
                                                                    style: UIAlertAction.Style.default) { (UIAlertAction) in
                        //retry logic
                        self?.requestDataFromApi()
                    }
                    
                    let actionNo : UIAlertAction = UIAlertAction.init(title: "No",
                                                                      style: UIAlertAction.Style.cancel,
                                                                      handler: nil)
                    
                    alertView.addAction(actionYes)
                    alertView.addAction(actionNo)
                    
                    self?.present(alertView, animated: true, completion: nil)
                }
            }
            else {
                let dbHandler = DatabaseHandler.shared
                self?.listOfSchools = dbHandler.querySchoolData() as! [School]
                
                print("Schools found : \(self?.listOfSchools.count ?? 0)")
                
                DispatchQueue.main.async {[weak self] in
                    //Update UI on main thread
                    busyIndicator.stopAnimating()
                    self?.tableView.reloadData()
                }
            }
        }
    }
}

