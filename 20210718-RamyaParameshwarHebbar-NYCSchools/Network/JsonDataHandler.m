//
//  JsonDataHandler.m
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/18/21.
//

#import "JsonDataHandler.h"
#import "DatabaseHandler.h"
#import "AppConstants.h"

@implementation JsonDataHandler

+ (instancetype) sharedHandler {
    static JsonDataHandler * sharedJsonDataHandler = nil;

        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedJsonDataHandler = [[self alloc] init];
        });
    
        return sharedJsonDataHandler;
}

- (instancetype) init {
    if (self = [super init]) {
        
    }
    NSLog(@"JsonDataHandler initialized");
    return self;
}

-(BOOL)fetchSchoolsData {
    
    NSString * urlString = [NSString stringWithFormat:@"%@%@?%@%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@&%@%@",
                            baseURL,
                            schoolsOffset,
                            querySelect,
                            fieldDBN,
                            fieldSchoolname,
                            fieldOverview,
                            fieldCity,
                            fieldNeighborhood,
                            fieldPhone,
                            fieldFax,
                            fieldEmail,
                            fieldWebsite,
                            fieldAddress,
                            fieldZip,
                            fieldState,
                            queryOrderBy,
                            fieldSchoolname];
        
    NSError *error = nil;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]
                                          options:0
                                            error:&error];
    
    if(error) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    else {
        id object = [NSJSONSerialization
                     JSONObjectWithData:data
                     options:0
                     error:&error];
        
        if(error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            return NO;
        }
        
        if([object isKindOfClass:[NSArray class]]) {
            //received list of schools
            NSArray * listOfSchools = (NSArray *)object;
            
            if(listOfSchools.count > 0) {
            
                //clearing old data
                [[DatabaseHandler sharedHandler] clearSchoolData];
                
                //loop through each object and store
                for(NSDictionary * dictionary in listOfSchools) {
                    [[DatabaseHandler sharedHandler] insertSchoolData:dictionary];
                }
                
                return YES;
            }
        }
        else {
            NSLog(@"Error parsing JSON data from given URL");
            return NO;
        }
    }
    
    return NO;
}

- (SchoolScore *) fetchSchoolDetailsFor:(NSString *)schoolID {
    NSString * urlString = [NSString stringWithFormat:@"%@%@?%@=%@",
                            baseURL,
                            scoresOffset,
                            fieldDBN,
                            schoolID];
    
    NSError *error = nil;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]
                                          options:0
                                            error:&error];
    
    if(error) {
        NSLog(@"%@", [error localizedDescription]);
        return nil;
    }
    else {
        id object = [NSJSONSerialization
                     JSONObjectWithData:data
                     options:0
                     error:&error];
        
        if(error) {
            NSLog(@"Error getting object data: %@", [error localizedDescription]);
            return nil;
        }
        
        if([object isKindOfClass:[NSArray class]]) {
            NSArray * schoolInfo = (NSArray *)object;
            NSDictionary * info = [schoolInfo firstObject];
            
            SchoolScore * score = [[SchoolScore alloc] init];
            score.schoolID = [info valueForKey:fieldDBN];
            score.readingScore = [info valueForKey:fieldScoreRead];
            score.mathScore = [info valueForKey:fieldScoreMath];
            score.writingScore = [info valueForKey:fieldScoreWrite];
            
            return score;
        }
    }
    
    return nil;
}

@end
