//
//  JsonDataHandler.h
//  20210718-RamyaParameshwarHebbar-NYCSchools
//
//  Created by Ramya Hebbar on 7/18/21.
//

#import <Foundation/Foundation.h>
#import "SchoolScore.h"

/*
 This class is responsible for fetching and parsing data from the server and providing the model objects.
 */

NS_ASSUME_NONNULL_BEGIN

@interface JsonDataHandler : NSObject

@property (class, readonly, strong) JsonDataHandler *sharedHandler NS_SWIFT_NAME(shared);

/*
 This method fetches all the NYC schools and stores them in the SQLite database.
 */

- (BOOL) fetchSchoolsData;

/*
 This method fetches the SAT scores for the selected school ID.
 */

- (SchoolScore *) fetchSchoolDetailsFor:(NSString *)schoolID;

@end

NS_ASSUME_NONNULL_END
